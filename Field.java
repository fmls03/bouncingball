import java.awt.*; 
import java.util.concurrent.TimeUnit;
import java.util.Random;

import java.awt.event.*;

public class Field {
	
	public static int h = 400;
	public static int w = 400;

	public Field(){
		

		Frame f = new Frame("my Field");
		f.add(new MyCanvas());
		f.setLayout(null);
		f.setSize(Field.w, Field.h);
		f.setVisible(true);

	}


	public static void main(String args[]) throws InterruptedException{

		new Field();

	}

	
}

class MyCanvas extends Canvas   
{    
        public MyCanvas() {    
        setBackground (Color.BLACK);    
        setSize(Field.w,Field.h);    
     }    
  
  public void paint(Graphics g)
  {    
  
  	int x = Field.w/2 + 10;
  	int y = Field.h/2 + 10;
  	int m = 1;
  	int q = 50;
 	
  	Random random = new Random();

 		int dx = random.nextInt(7);
 		int dy = random.nextInt(7);

    g.setColor(Color.green);
    g.fillOval(x,y,5,5);

    int i = 1;
    
    

    while (i != 0) {
	
			try{

		    g.clearRect(0,0,Field.w,Field.h);
		    g.fillOval(x,y,5,5);
				x = x + dx;
				y = y + dy;
				if(x >= (400-5) || x <= 5){
					m = m*-1;
					dx = dx* -1;
				}
				if (y >= (400-5) || y <= 25){
					dy = dy * -1;
				}
				Thread.sleep(100);

			}
			catch(InterruptedException ex){
				ex.printStackTrace();
			}
	  }    
	} 
}


