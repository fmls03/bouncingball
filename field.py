from tkinter import *
import time
import random
import math


w = 400

h = 400
x = 7
r = 4
m = 1.2

window = Tk()
window.geometry("400x400")


field = Canvas(window, width=(w), height=(h))

field.pack()
vx = 1
vy = 1

a = 30

q = 10

dx = 1
dy = 1

y = (m*x) + q

hit = 1
formula = ''

def calcoloRetta(x,q):
    return ((m*x) + q)


def create_circle(x,y,r, canvas):
    x0 = x - r
    y0 = y - r
        
    x1 = x + r
    y1 = y + r
    return canvas.create_oval(x0,y0,x1,y1)



def bouncing():
    global x,y,m,q,dx, dy, hit
    create_circle(x,y,r,field)
    if hit == 1:
        y = calcoloRetta(x,q)
        x += (1*m)
    elif hit == -1:
        x = calcoloRetta(y,q)
        y += (1*m)

    if (y) >= (h-r):
        hit = -1
        y = h-r
        dy*= -1
    elif (x+r) >= w:
        hit = 1
        x = w-r
        dx *= -1
    elif (x-r) <= 0:
        hit = 1
        x = r
        dx *= -1
    elif (y+r) <= 0:
        hit = -1
        y = r
        dy*= -1

def run():
    global x,y,vx,vy
    bouncing()
    time.sleep(0.01)
    field.delete('all')
    return create_circle(x,y,r,field)

while True:
    run()
    print(x,y,m)
    window.update()

window.mainloop()
